import typer


def add(x: int, y: int) -> int:
    return x + y


def main(x: int, y: int) -> None:
    typer.echo(f"{x}+{y}={add(x,y)}")


if __name__ == "__main__":
    typer.run(main)
