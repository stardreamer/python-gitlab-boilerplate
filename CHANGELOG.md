# Changelog

## [0.2.0] - 02.08.2020
 
### Added

* Stylechecks and issue templates were added [#2](https://gitlab.com/stardreamer/python-gitlab-boilerplate/-/issues/2)

## [0.1.0] - 02.08.2020
 
### Added

* Repository was created [#1](https://gitlab.com/stardreamer/python-gitlab-boilerplate/-/issues/1)