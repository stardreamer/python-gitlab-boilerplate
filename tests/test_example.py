import pytest

from example_pkg_43.main import add


@pytest.mark.parametrize("x,y,res", [(0, 1, 1), (1, 1, 2)])
def test_add(x: int, y: int, res: int) -> None:
    assert add(x, y) == res
