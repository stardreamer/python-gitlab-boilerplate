# Python GitLab Boilerplate

The goal of this repository is to provide an example of how you can organize your python development workflow with poetry and GitLab.

## Suggested workflow

1. Create an issue using the appropriate template.
2. Create a branch and a merge request regarding this issue.
3. Make the necessary changes.
4. Bump the version of your package with the `poetry version` command.
5. Wait for your merge request to be accepted.
6. Check that deploy and release steps of the resulting pipeline was successful.
