## Description:

(Feature description)

## Main Idea:

(Here you can give a more detailed explanation of the requested feature)

## Feature requirements:

1.
2.
3.
4.


## Extra information:

(e.g. related issues, suggestions how to implement, links for us to have context, eg. stackoverflow, gitter, etc)


/label ~feature